# How long has been since last Facebook scandal? Find out!

This is an experiment you can contribute to! Currently, the website is very simple and we update it through Gitlab, if you know better solutions, we would be happy to hear from you! Especially if it allows people to contribute without using Gitlab or Git or coding raw HTML.

The rest is pretty simple, it is just an HTML page.
  
The website can be accessed at [https://dayssincelastfacebookscandal.com/](https://dayssincelastfacebookscandal.com/)
  
Code under: CC-BY-SA
