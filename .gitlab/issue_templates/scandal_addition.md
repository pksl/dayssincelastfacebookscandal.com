Title

(Title of the scandal. Eg: "Facebook against the workers rights")

Date

(Date of the fist scandal occurence. Eg: 12/11/2020)

Sources

(list of all the sources/news websites (with links) that covered the scandal)

/label ~scandal ~addition
/assign @jae
/cc @jae